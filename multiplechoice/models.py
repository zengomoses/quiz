from django.db import models

# Create your models here.
from django.utils.encoding import python_2_unicode_compatible

from quiz.models import Question
from django.utils.translation import ugettext_lazy as _

ANSWER_ORDER_OPTIONS = (
    ('option', _('Option')),
    ('content', _('Content')),
    ('random', _('Random')),
    ('none', _('None')),
)


class MCQuestion(Question):
    answer_order = models.CharField(
        max_length=30, null=True, blank=True,
        choices=ANSWER_ORDER_OPTIONS,
        help_text=_("The order in which multiple choice answer "
                    "options are displayed to the user"),
        verbose_name=_("Answer Order")
    )

    class Meta:
        verbose_name = _("Multiple Choice Question")
        verbose_name_plural = _("Multiple Choice Questions")

    def check_if_correct(self, guess):
        answer = Answer.objects.get(id=guess)

        if answer.correct is True:
            return True
        else:
            return False

    def order_answers(self, queryset):
        if self.answer_order == 'option':
            return queryset.order_by('option')
        if self.answer_order == 'content':
            return queryset.order_by('content')
        if self.answer_order == 'random':
            return queryset.order_by('?')
        if self.answer_order == 'none':
            return queryset.order_by()
        return queryset

    def get_answers(self):
        return self.order_answers(Answer.objects.filter(question=self))

    def get_answer_list(self):
        return [(answer.id, answer.content) for answer in self.order_answers(Answer.objects.filter(question=self))]

    def answer_choice_to_string(self, guess):
        return Answer.objects.get(id=guess).content


@python_2_unicode_compatible
class Answer(models.Model):
    OPTIONS = (
        ('A', _('A')),
        ('B', _('B')),
        ('C', _('C')),
        ('D', _('D')),
        ('E', _('E'))
    )
    question = models.ForeignKey(MCQuestion, verbose_name=_("Question"), related_name="question_answer", on_delete=models.CASCADE)
    option = models.CharField(max_length=1, choices=OPTIONS, blank=True, null=True)
    content = models.CharField(max_length=1000, blank=False,
                               help_text=_("Enter the answer text that you want displayed"),
                               verbose_name=_("Content"))
    correct = models.BooleanField(blank=False, default=False, help_text=_("Is this a correct answer?"))

    class Meta:
        verbose_name = _("Answer")
        verbose_name_plural = _("Answers")

    def __str__(self):
        return self.content
