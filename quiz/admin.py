from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms import ModelForm, ModelMultipleChoiceField

from multiplechoice.models import Answer, MCQuestion
from quiz.models import Quiz, Question, Course, CourseLevel, Subject, Topic
from django.utils.translation import ugettext_lazy as _


class AnswerInline(admin.TabularInline):
    model = Answer


class QuizAdminForm(ModelForm):
    class Meta:
        model = Quiz
        exclude = []

    questions = ModelMultipleChoiceField(
        queryset=Question.objects.all().select_subclasses(),
        required=False,
        label=_("Questions"),
        widget=FilteredSelectMultiple(
            verbose_name=_("Questions"),
            is_stacked=False
        )
    )

    def __init__(self, *args, **kwargs):
        super(QuizAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['questions'].initial = self.instance.question_set.all().select_subclasses()

    def save(self, commit=True):
        quiz = super(QuizAdminForm, self).save(commit=False)
        quiz.save()
        quiz.question_set.set(self.cleaned_data['questions'])
        self._save_m2m()
        return quiz


class QuizAdmin(admin.ModelAdmin):
    form = QuizAdminForm

    list_display = ('title', 'course',)
    list_filter = ('course',)
    search_fields = ('description', 'course',)


class CourseAdmin(admin.ModelAdmin):
    search_fields = ('course',)


class SubjectAdmin(admin.ModelAdmin):
    search_fields = ('code', 'name',)
    fields = ('code', 'name', 'course_level')
    list_display = ('code', 'name', 'course_level', 'url')


class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'subject',)
    search_fields = ('name', 'subject',)


class CourseLevelAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'course',)
    list_filter = ('course',)


class MCQuestionAdmin(admin.ModelAdmin):
    list_display = ('content', 'topic', 'course_level')
    list_filter = ('course', 'course_level', 'topic')
    fields = ('content', 'course', 'course_level', 'topic', 'figure', 'explanation', 'answer_order')
    search_fields = ('content', 'explanation')
    filter_horizontal = ('quiz',)
    inlines = [AnswerInline]


admin.site.register(Quiz, QuizAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(CourseLevel, CourseLevelAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(MCQuestion, MCQuestionAdmin)
