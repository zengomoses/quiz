import json
import re

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, ImproperlyConfigured
from django.core.validators import MaxValueValidator, validate_comma_separated_integer_list
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from model_utils.managers import InheritanceManager


class CourseManager(models.Manager):

    def new_course(self, course):
        new_course = self.create(course=re.sub('\s+', '-', course).lower())

        new_course.save()
        return new_course


@python_2_unicode_compatible
class Course(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("Course"))

    objects = CourseManager()

    class Meta:
        verbose_name = _("Course")
        verbose_name_plural = _("Courses")

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class CourseLevel(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("Name"))
    course = models.ForeignKey(Course, null=True, related_name='course_level', blank=True, verbose_name=_("Course"), on_delete=models.CASCADE)
    url = models.SlugField(verbose_name=_("User friendly url"), max_length=200, blank=True, null=True)

    objects = CourseManager()

    class Meta:
        verbose_name = _("Course Level")
        verbose_name_plural = _("Course Levels")

    def __str__(self):
        return "{}".format(self.name + " (" + self.course.name + ")")

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        self.url = slugify(self.name.lower())
        self.url = "%s" % self.url

        super(CourseLevel, self).save(force_insert, force_update, *args, **kwargs)


@python_2_unicode_compatible
class Subject(models.Model):
    code = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    course_level = models.ForeignKey(CourseLevel, related_name='subject_course_level', on_delete=models.CASCADE)
    url = models.SlugField(verbose_name=_("User friendly url"), max_length=200, blank=True, null=True)

    class Meta:
        verbose_name = _("Subject")
        verbose_name_plural = _("Subjects")

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        self.url = slugify(self.name.lower())
        self.url = "%s" % self.url

        super(Subject, self).save(force_insert, force_update, *args, **kwargs)


@python_2_unicode_compatible
class Topic(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    url = models.SlugField(verbose_name=_("User friendly url"), max_length=200, blank=True, null=True)

    class Meta:
        verbose_name = _("Topic")
        verbose_name_plural = _("Topics")

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        self.url = slugify(self.name.lower())
        self.url = "%s" % self.url

        super(Topic, self).save(force_insert, force_update, *args, **kwargs)


class Quiz(models.Model):
    title = models.CharField(verbose_name=_("Title"),
                             max_length=200, blank=True, null=True)

    description = models.TextField(
        verbose_name=_("Description"), blank=True,
        help_text=_("A description of the text"))

    url = models.SlugField(verbose_name=_("User friendly url"), max_length=200, blank=True, null=True)

    course = models.ForeignKey(
        Course, null=True, blank=True,
        verbose_name=_("Course"), on_delete=models.CASCADE)

    course_level = models.ForeignKey(
        CourseLevel, null=True, blank=True,
        verbose_name=_("Course Level"), on_delete=models.CASCADE)

    subject = models.ForeignKey(
        Subject, null=True, blank=True,
        verbose_name=_("Subject"), on_delete=models.CASCADE)

    topic = models.ForeignKey(
        Topic, null=True, blank=True,
        verbose_name=_("Topic"), on_delete=models.CASCADE)

    random_order = models.BooleanField(
        blank=False, default=False,
        verbose_name=_("Random Order"),
        help_text=_("Display the questions in "
                    "a random order or as they "
                    "are set?"))

    max_questions = models.PositiveIntegerField(
        blank=True, null=True, verbose_name=_("Max Questions"),
        help_text=_("Number of questions to be answered on each attempt."))

    answers_at_end = models.BooleanField(
        blank=False, default=False,
        help_text=_("Correct answer is NOT shown after question."
                    " Answers displayed at the end."),
        verbose_name=_("Answers at end"))

    exam_paper = models.BooleanField(
        blank=False, default=False,
        help_text=_("If yes, the result of each"
                    " attempt by a user will be"
                    " stored. Necessary for marking."),
        verbose_name=_("Exam Paper"))

    single_attempt = models.BooleanField(
        blank=False, default=False,
        help_text=_("If yes, only one attempt by"
                    " a user will be permitted."
                    " Non users cannot sit this exam."),
        verbose_name=_("Single Attempt"))

    pass_mark = models.SmallIntegerField(
        blank=True, default=0,
        verbose_name=_("Pass Mark"),
        help_text=_("Percentage required to pass exam."),
        validators=[MaxValueValidator(100)])

    success_text = models.TextField(
        blank=True, help_text=_("Displayed if user passes."),
        verbose_name=_("Success Text"))

    fail_text = models.TextField(
        verbose_name=_("Fail Text"),
        blank=True, help_text=_("Displayed if user fails."))

    draft = models.BooleanField(
        blank=True, default=False,
        verbose_name=_("Draft"),
        help_text=_("If yes, the quiz is not displayed"
                    " in the quiz list and can only be"
                    " taken by users who can edit"
                    " quizzes."))
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Quiz")
        verbose_name_plural = _("Quizzes")
        get_latest_by = 'date_created'

    def __str__(self):
        return self.title

    def get_questions(self):
        return self.question_set.all().select_subclasses()

    @property
    def get_max_score(self):
        return self.get_questions().count()

    def anon_score_id(self):
        return str(self.id) + "_score"

    def anon_q_list(self):
        return str(self.id) + "_q_list"

    def anon_q_data(self):
        return str(self.id) + "_data"

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        self.url = slugify(self.title.lower())
        self.url = "%s" % self.url
        if self.single_attempt is True:
            self.exam_paper = True

        if self.pass_mark > 100:
            raise ValidationError('%s is above 100' % self.pass_mark)

        super(Quiz, self).save(force_insert, force_update, *args, **kwargs)


class ProgressManager(models.Manager):

    def new_progress(self, user):
        new_progress = self.create(user=user, score="")
        new_progress.save()
        return new_progress


class Progress(models.Model):
    user = models.OneToOneField(User, verbose_name=_("User"), on_delete=models.CASCADE)
    score = models.CharField(max_length=1024, verbose_name=_("Score"),
                             validators=[validate_comma_separated_integer_list])
    objects = ProgressManager()

    class Meta:
        verbose_name = _("User Progress")
        verbose_name_plural = _("User Progress Records")

    def list_all_course_scores(self):
        score_before = self.score
        output = {}

        for course in Course.objects.all():
            to_find = re.escape(course.name) + r",(\d+),(\d+),"
            match = re.search(to_find, self.score, re.IGNORECASE)

            if match:
                score = int(match.group(1))
                possible = int(match.group(2))

                try:
                    percent = int(round((float(score) / float(possible)) * 100))
                except:
                    percent = 0
                output[course.name] = [score, possible, percent]
            else:
                self.score += course.name + ",0,0,"
                output[course.name] = [0, 0]

        if len(self.score) > len(score_before):
            self.save()

        return output

    def update_score(self, question, score_to_add=0, possible_to_add=0):
        """
        Pass in question object, amount to increase score
        and max possible.

        Does not return anything.
        """
        course_test = Course.objects.filter(name=question.course) \
            .exists()

        if any([item is False for item in [course_test,
                                           score_to_add,
                                           possible_to_add,
                                           isinstance(score_to_add, int),
                                           isinstance(possible_to_add, int)]]):
            return _("error"), _("category does not exist or invalid score")

        to_find = re.escape(str(question.course)) + \
                  r",(?P<score>\d+),(?P<possible>\d+),"

        match = re.search(to_find, self.score, re.IGNORECASE)

        if match:
            updated_score = int(match.group('score')) + abs(score_to_add)
            updated_possible = int(match.group('possible')) + \
                               abs(possible_to_add)

            new_score = ",".join(
                [
                    str(question.course),
                    str(updated_score),
                    str(updated_possible), ""
                ])

            # swap old score for the new one
            self.score = self.score.replace(match.group(), new_score)
            self.save()

        else:
            #  if not present but existing, add with the points passed in
            self.score += ",".join(
                [
                    str(question.course),
                    str(score_to_add),
                    str(possible_to_add),
                    ""
                ])
            self.save()

    def show_exams(self):
        """
        Finds the previous quizzes marked as 'exam papers'.
        Returns a queryset of complete exams.
        """
        return Sitting.objects.filter(user=self.user, complete=True)


class SittingManager(models.Manager):

    def new_sitting(self, user, quiz):
        if quiz.random_order is True:
            question_set = quiz.question_set.all().select_subclasses().order_by('?')
        else:
            question_set = quiz.question_set.all().select_subclasses()

        question_set = [item.id for item in question_set]

        if len(question_set) == 0:
            raise ImproperlyConfigured("Question set of the quiz is empty. "
                                       "Please configure questions properly")

        if quiz.max_questions and quiz.max_questions < len(question_set):
            question_set = question_set[:quiz.max_questions]

            questions = ",".join(map(str, question_set)) + ","

            new_sitting = self.create(user=user,
                                      quiz=quiz,
                                      question_order=questions,
                                      question_list=questions,
                                      incorrect_questions="",
                                      current_score=0,
                                      complete=False,
                                      user_answers='{}')

            return new_sitting

    def user_sitting(self, user, quiz):
        if quiz.single_attempt is True and self.filter(user=user, quiz=quiz, complete=True).exists():
            return False

        try:
            sitting = self.get(user=user, quiz=quiz, complete=False)
        except Sitting.DoesNotExist:
            sitting = self.new_sitting(user, quiz)
        except Sitting.MultipleObjectsReturned:
            sitting = self.filter(user=user, quiz=quiz, complete=False)[0]

        return sitting


class Sitting(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_('User'))
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, verbose_name=_('Quiz'))
    question_order = models.CharField(max_length=1024, verbose_name=_('Question Order'),
                                      validators=[validate_comma_separated_integer_list])
    question_list = models.CharField(max_length=1024, verbose_name=_('Question List'),
                                     validators=[validate_comma_separated_integer_list])
    incorrect_questions = models.CharField(max_length=1024, verbose_name=_('Incorrect Questions'),
                                           validators=[validate_comma_separated_integer_list])
    complete = models.BooleanField(default=False, blank=False, verbose_name=_("Complete"))
    current_score = models.IntegerField(verbose_name=_("Current Score"))
    user_answers = models.TextField(blank=True, default='{}', verbose_name=_("User Answers"))
    start = models.DateTimeField(auto_now_add=True, verbose_name=_("Start"))
    end = models.DateTimeField(null=True, blank=True, verbose_name=_("End"))

    objects = SittingManager()

    class Meta:
        permissions = (("view_sittings", _("Can see completed exams,")),)

    def get_first_question(self):
        """
        :return:
        Returns the next question.
        if no question is found, return False
        Does NOT remove the question from the front of the list
        """

        if not self.question_list:
            return False

        first, _ = self.question_list.split(',', 1)
        question_id = int(first)
        return Question.objects.get_subclass(id=question_id)

    def remove_first_question(self):
        if not self.question_list:
            return

        _, others = self.question_list.split(',', 1)
        self.question_list = others
        self.save()

    def add_to_score(self, points):
        self.current_score += int(points)
        self.save()

    @property
    def get_current_score(self):
        return self.current_score

    def _question_ids(self):
        return [int(n) for n in self.question_order.split(',') if n]

    @property
    def get_percent_correct(self):
        dividend = float(self.current_score)
        divisor = len(self._question_ids())

        if divisor < 1:
            return 0  # prevent divide by zero error

        if dividend > divisor:
            return 100

        correct = int(round((dividend / divisor) * 100))

        if correct >= 1:
            return correct
        else:
            return 0

    def mark_quiz_complete(self):
        self.complete = True
        self.end = now()
        self.save()

    def add_incorrect_question(self, question):
        """
        Adds uid of incorrect question to the list.
        The question object must be passed in
        :param question:
        :return:
        """
        if len(self.incorrect_questions) > 0:
            self.incorrect_questions += ','
        self.incorrect_questions += str(question.id) + ","
        if self.complete:
            self.add_to_score(-1)
        self.save()

    @property
    def get_incorrect_questions(self):
        """
        :return: incorrect_questions
        Returns a list of non empty integers, representing the pk of questions
        """
        incorrect_questions = [int(q) for q in self.incorrect_questions.split(',') if q]
        return incorrect_questions

    def remove_incorrect_question(self, question):
        current = self.get_incorrect_questions
        current.remove(question.id)
        self.incorrect_questions = ','.join(map(str, current))
        self.add_to_score(1)
        self.save()

    @property
    def check_if_passed(self):
        return self.get_percent_correct >= self.quiz.pass_mark

    @property
    def result_message(self):
        if self.check_if_passed:
            return self.quiz.success_text
        else:
            return self.quiz.fail_text

    def add_user_answer(self, question, guess):
        current = json.loads(self.user_answers)
        current[question.id] = guess
        self.user_answers = json.dumps(current)
        self.save()

    def get_questions(self, with_answers=False):
        question_ids = self._question_ids()
        questions = sorted(
            self.quiz.question_set.filter(id__in=question_ids).select_subclasses(),
            key=lambda q: question_ids.index(q.id))

        if with_answers:
            user_answers = json.loads(self.user_answers)
            for question in questions:
                question.user_answer = user_answers[str(question.id)]

        return questions

    @property
    def questions_with_user_answers(self):
        return {
            q: q.user_answer for q in self.get_questions(with_answers=True)
        }

    def get_max_score(self):
        return len(self._question_ids())

    def progress(self):
        """
        Returns the number of questions answered so far and the total number of questions.
        :return:
        """
        answered = len(json.loads(self.user_answers))
        total = self.get_max_score()

        return answered, total


@python_2_unicode_compatible
class Question(models.Model):
    """
    Base class for all question types. Shared properties placed here.
    """
    quiz = models.ManyToManyField(Quiz,
                                  verbose_name=_("Quiz"),
                                  blank=True)

    course = models.ForeignKey(Course,
                               verbose_name=_("Course"),
                               blank=True,
                               null=True,
                               on_delete=models.CASCADE)

    course_level = models.ForeignKey(CourseLevel,
                                     verbose_name=_("Course-Level"),
                                     blank=True,
                                     null=True,
                                     on_delete=models.CASCADE)

    topic = models.ForeignKey(Topic,
                              verbose_name=_("Topic"),
                              blank=True,
                              null=True,
                              on_delete=models.CASCADE)

    figure = models.ImageField(upload_to='uploads/%Y/%m/%d',
                               blank=True,
                               null=True,
                               verbose_name=_("Figure"))

    content = models.TextField(max_length=2000,
                               blank=False,
                               help_text=_("Enter the question text that "
                                           "you want displayed"),
                               verbose_name=_('Question'))

    explanation = models.TextField(max_length=2000,
                                   blank=True,
                                   help_text=_("Explanation to be shown "
                                               "after the question has "
                                               "been answered."),
                                   verbose_name=_('Explanation'))

    objects = InheritanceManager()

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")
        ordering = ['course']

    def __str__(self):
        return self.content
