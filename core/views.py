import random
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.generic import TemplateView, ListView, DetailView, FormView
from quiz.forms import QuestionForm
from quiz.models import CourseLevel, Subject, Quiz, Sitting, Progress, Question, Topic


class HomeView(TemplateView):
    template_name = 'index.html'


class CPAIndexView(TemplateView):
    template_name = 'cpa/index.html'

    def get_context_data(self, **kwargs):
        context = super(CPAIndexView, self).get_context_data(**kwargs)
        context['levels'] = CourseLevel.objects.filter(course__name='CPA')
        return context


class CISAIndexView(TemplateView):
    template_name = 'cisa/index.html'

    def get_context_data(self, **kwargs):
        context = super(CISAIndexView, self).get_context_data(**kwargs)
        context['levels'] = CourseLevel.objects.filter(course__name='CISA')
        return context


class ACCAIndexView(TemplateView):
    template_name = 'acca/index.html'

    def get_context_data(self, **kwargs):
        context = super(ACCAIndexView, self).get_context_data(**kwargs)
        context['levels'] = CourseLevel.objects.filter(course__name='ACCA')
        return context


class CBPIndexView(TemplateView):
    template_name = 'cbp/index.html'

    def get_context_data(self, **kwargs):
        context = super(CBPIndexView, self).get_context_data(**kwargs)
        context['levels'] = CourseLevel.objects.filter(course__name='CBP')
        return context


def get_level_subjects(request):
    if request.method == 'GET':
        level_id = request.GET.get('level_id')
        course_level = CourseLevel.objects.get(id=level_id)
        queryset = Subject.objects.filter(course_level=course_level).values()

        data = {"subjects": list(queryset)}
        print(data)
        return JsonResponse(data)


class QuizListView(ListView):
    template_name = 'quiz_list.html'
    context_object_name = 'quizes'

    def get_queryset(self):
        queryset = Quiz.objects.filter(subject__url=self.request.GET.get('subject'))
        return queryset

    def get_subject(self):
        queryset = Subject.objects.get(url=self.request.GET.get('subject'))

        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(QuizListView, self).get_context_data(**kwargs)
        context['subject'] = self.get_subject().name
        context['course'] = self.get_subject().course_level.name
        context['subjects'] = Subject.objects.filter(course_level=self.get_subject().course_level)
        return context


class QuizDetailView(DetailView):
    model = Quiz
    template_name = "quiz_detail.html"
    context_object_name = "quiz"
    slug_field = 'url'


class TakeQuizView(FormView):
    form_class = QuestionForm
    template_name = 'questions.html'
    result_template_name = 'result.html'
    single_complete_template_name = 'single_complete.html'

    def dispatch(self, request, *args, **kwargs):
        self.quiz = get_object_or_404(Quiz, url=self.kwargs['quiz_name'])
        if self.quiz.draft and not request.user.has_perm('quiz.change_quiz'):
            raise PermissionDenied
        try:
            self.logged_in_user = self.request.user.is_authenticated()
        except TypeError:
            self.logged_in_user = self.request.user.is_authenticated

        if self.logged_in_user:
            self.sitting = Sitting.objects.user_sitting(self.request.user, self.quiz)
            print("Sitting: ", self.sitting.get_first_question())
        else:
            self.sitting = self.anon_load_sitting()

        if self.sitting is False:
            return render(request, self.single_complete_template_name)

        return super(TakeQuizView, self).dispatch(request, *args, **kwargs)

    def get_form(self, *args, **kwargs):
        if self.logged_in_user:
            self.question = self.sitting.get_first_question()
            self.progress = self.sitting.progress()
        else:
            self.question = self.anon_next_question()
            self.progress = self.anon_sitting_progress()

        form_class = self.form_class

        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(TakeQuizView, self).get_form_kwargs()

        return dict(kwargs, question=self.question)

    def form_valid(self, form):
        if self.logged_in_user:
            self.form_valid_user(form)
            if self.sitting.get_first_question() is False:
                return self.final_result_user()
        else:
            self.form_valid_anon(form)
            if not self.request.session[self.quiz.anon_q_list()]:
                return self.final_result_anon()
        self.request.POST = {}

        return super(TakeQuizView, self).get(self, self.request)

    def get_context_data(self, **kwargs):
        context = super(TakeQuizView, self).get_context_data(**kwargs)
        context['question'] = self.question
        context['quiz'] = self.quiz
        if hasattr(self, 'previous'):
            context['previous'] = self.previous
        if hasattr(self, 'progress'):
            context['progress'] = self.progress

        return context

    def form_valid_user(self, form):
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct is True:
            self.sitting.add_to_score(1)
            progress.update_score(self.question, 1, 1)
        else:
            self.sitting.add_incorrect_question(self.question)
            progress.update_score(self.question, 0, 1)

        if self.quiz.answers_at_end is not True:
            self.previous = {
                'previous_answer': guess,
                'previous_outcome': is_correct,
                'previous_question': self.question,
                'answer': self.question.get_answers(),
                'question_type': {self.question.__class__.__name__: True}
            }
        else:
            self.previous = {}
        self.sitting.add_user_answer(self.question, guess)
        self.sitting.remove_first_question()

    def final_result_user(self):
        results = {
            'quiz': self.quiz,
            'score': self.sitting.get_current_score,
            'max_score': self.sitting.get_max_score,
            'percent': self.sitting.get_percent_correct,
            'sitting': self.sitting,
            'previous': self.previous,
        }

        self.sitting.mark_quiz_complete()

        if self.quiz.answers_at_end:
            results['questions'] = \
                self.sitting.get_questions(with_answers=True)
            results['incorrect_questions'] = \
                self.sitting.get_incorrect_questions

        if self.quiz.exam_paper is False:
            self.sitting.delete()

        return render(self.request, self.result_template_name, results)

    def anon_load_sitting(self):
        if self.quiz.single_attempt is True:
            return False

        if self.quiz.anon_q_list() in self.request.session:
            return self.request.session[self.quiz.anon_q_list()]
        else:
            return self.new_anon_quiz_session()

    def new_anon_quiz_session(self):
        """
        Sets the session variables when starting a quiz for the first time
        as a non signed-in user
        """
        self.request.session.set_expiry(259200)  # expires after 3 days
        questions = self.quiz.get_questions()
        question_list = [question.id for question in questions]

        if self.quiz.random_order is True:
            random.shuffle(question_list)

        if self.quiz.max_questions and (self.quiz.max_questions
                                        < len(question_list)):
            question_list = question_list[:self.quiz.max_questions]

        # session score for anon users
        self.request.session[self.quiz.anon_score_id()] = 0

        # session list of questions
        self.request.session[self.quiz.anon_q_list()] = question_list

        # session list of question order and incorrect questions
        self.request.session[self.quiz.anon_q_data()] = dict(
            incorrect_questions=[],
            order=question_list,
        )

        return self.request.session[self.quiz.anon_q_list()]

    def anon_next_question(self):
        next_question_id = self.request.session[self.quiz.anon_q_list()][0]
        return Question.objects.get_subclass(id=next_question_id)

    def anon_sitting_progress(self):
        total = len(self.request.session[self.quiz.anon_q_data()]['order'])
        answered = total - len(self.request.session[self.quiz.anon_q_list()])
        return answered, total

    def form_valid_anon(self, form):
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct:
            self.request.session[self.quiz.anon_score_id()] += 1
            anon_session_score(self.request.session, 1, 1)
        else:
            anon_session_score(self.request.session, 0, 1)
            self.request.session[self.quiz.anon_q_data()]['incorrect_questions'].append(self.question.id)

        self.user_answers = {}
        self.previous = {}

        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question.__class__.__name__: True}}

        self.request.session[self.quiz.anon_q_list()] =\
            self.request.session[self.quiz.anon_q_list()][1:]

    def final_result_anon(self):
        quiz = self.quiz
        score = self.request.session[self.quiz.anon_score_id()]
        q_order = self.request.session[self.quiz.anon_q_data()]['order']
        max_score = len(q_order)
        percent = int(round((float(score) / max_score) * 100))
        session, session_possible = anon_session_score(self.request.session)
        if score is 0:
            score = "0"
        print(session)
        results = {
            'quiz': quiz,
            'score': score,
            'max_score': max_score,
            'percent': percent,
            'session': session,
            'possible': session_possible,
        }

        del self.request.session[self.quiz.anon_q_list()]

        if self.quiz.answers_at_end:
            results['questions'] = sorted(
                self.quiz.question_set.filter(id__in=q_order)
                                      .select_subclasses(),
                key=lambda q: q_order.index(q.id))

            results['incorrect_questions'] = (
                self.request.session[self.quiz.anon_q_data()]['incorrect_questions'])

            results['previous'] = self.previous

        else:
            results['previous'] = self.previous

        del self.request.session[self.quiz.anon_q_data()]

        return render(self.request, 'result.html', results)


def anon_session_score(session, to_add=0, possible=0):
    """
    Returns the session score for non-signed in users.
    If number passed in then add this to the running total and
    return session score.

    examples:
        anon_session_score(1, 1) will add 1 out of a possible 1
        anon_session_score(0, 2) will add 0 out of a possible 2
        x, y = anon_session_score() will return the session score
                                    without modification

    Left this as an individual function for unit testing
    """
    if "session_score" not in session:
        session["session_score"], session["session_score_possible"] = 0, 0

    if possible > 0:
        session["session_score"] += to_add
        session["session_score_possible"] += possible

    return session["session_score"], session["session_score_possible"]
