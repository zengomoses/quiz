"""Quiz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, re_path

from core import settings
from core.views import HomeView, CPAIndexView, get_level_subjects, QuizListView, TakeQuizView, QuizDetailView, \
    CISAIndexView, ACCAIndexView, CBPIndexView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HomeView.as_view(), name='home'),
    path('cpa', CPAIndexView.as_view(), name='cpa'),
    path('cisa', CISAIndexView.as_view(), name='cisa'),
    path('acca', ACCAIndexView.as_view(), name='acca'),
    path('cbp', CBPIndexView.as_view(), name='cbp'),
    re_path('^(?P<course_name>[\w-]+)/quiz$', QuizListView.as_view(), name='quiz'),
    path('cpa/quiz/take', TakeQuizView.as_view(), name='take_quiz'),
    url(r'^cpa/quiz/(?P<slug>[\w-]+)$', view=QuizDetailView.as_view(), name='quiz_detail'),
    url(r'^cpa/quiz/(?P<quiz_name>[\w-]+)/take/$', view=TakeQuizView.as_view(), name='quiz_question'),
    path('subjects', get_level_subjects, name='get_level_subjects')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "CORNERSTONE FINANCIAL CONSULTANTS"
admin.site.site_title = "CORNERSTONE EXAM PREP"
admin.site.index_title = "Welcome to CORNERSTONE EXAM PREPARATION"

