Django==2.2.4
django-model-utils==3.2.0
Pillow==6.1.0
pytz==2019.2
sqlparse==0.3.0
